.. index:: Bibliography
.. index:: References

References
**********

.. bibliography:: references.bib
   :all:
