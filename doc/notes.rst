.. index:: Limitations

Known limitations
*****************

* Sometimes problems can occur due to known FFT features such as
  folding and aliasing.

.. todo::
   Erik, please check this out and rewrite.
  
* I am not sure how `filon` integration works and deals with these
  problem but be aware that :program:`dynasor` does not obey Nyquist
  criterion and you should probably only look at frequencies lower
  than :math:`\frac{\omega_{max}}{2}`.

* One should take the absolute value of the frequency functions since
  FFT amplitudes can sometimes become negative.

* Only constant volume trajectories are supported.
