.. index:: Examples; aluminum
	   
Liquid and solid aluminum
*************************

In this first example aluminium will be studied. An EAM potential was
used [Phys. Rev. B 59, 3393 (1999)] to describe both the solid and
liquid phases. The melting point was found to be about 930 K. The
lattice parameters used for the NVT MD simulations were found running
NPT and averaging the box length.


.. _example_liquid_aluminum:

.. index:: Examples; aluminum, liquid

Liquid
======

First we look at alumnium above the melting point at 1400 K, i.e., as
a liquid using a cell comprising 2048 atom. For liquids a spherically
average over :math:`\boldsymbol{q}` is desirable. To compute static
properties such as :math:`S(q)` no time correlation needs to be
computed which reduces the tomputation time a lot. Therefore there are
two different bash scripts setupt that calls :program:`dynasor`,
`run_dynasor_static.sh` and `run_dynasor_dynamical.sh`. Note that
these scripts may take a while to run so starting with fewer
:math:`\mathrm{MAX\_FRAMES}` is probably good idea. Two plotting
scripts are also included in order to reproduce the figures seen here
below if running the :program:`dynasor` scripts as are.

In the figure the structure factor  :math:`S(q)` is shown.

.. figure:: figs/Al_liquid/Al_liquid_static.png
    :scale: 120 %
    :align: center

    Structure factor :math:`S(q)` computed by running
    `run_dynasor_static.sh`. The :math:`q=0` point has been skipped
    for the structure factor due to :math:`S(0) = N`, see theory.

The structure factor is in good agreement with other MD simulations as
well as with experimental data, see Figure 2 in :cite:`Mokshin`.

A map of the dynamical structure factor and the two current
correlations is seen below as a function of :math:`q` and
:math:`\omega`. The figure is cut at :math:`q=2.5nm^{-1}` because the
resolution in :math:`q`-space is very poor below this point.

The intensity in :math:`C_l(q,\omega)` is more pronounced than
:math:`S(q,\omega)`, which means longitudinal vibrations are more
easily observed in the current correlation rather than the dynamical
structure factor.
 
These heatmap seem to agree very well with spectral intensity plots in
Figure 3 of :cite:`Mokshin`.  The clear dispersion in
:math:`C_l(q,\omega)` agrees with the dispersion relation in Figure 4
of :cite:`Mokshin`.

.. figure:: figs/Al_liquid/Al_liquid_heatmap.png
    :scale: 120 %
    :align: center

    Dynamical structure factor (a), longitudinal current (b) and
    transverse current (c).


    
.. _example_crystalline_aluminum:

.. index:: Examples; aluminum, crystalline

Crystalline
===========

.. todo::
   Update these old figures.

In this example we look at alumnium below the melting
point at :math:`T=300K`. The crystal structure is face-centered-cubic
(fcc) and :math:`q`-space sampling was done along three paths

.. math:: L=\frac{2\pi}{a}
          \Big(\frac{1}{2},\frac{1}{2},\frac{1}{2}\Big) \quad , \quad
          K=\frac{2\pi}{a} \Big(\frac{3}{4},\frac{3}{4},0\Big)

Since the sampling along a path is much faster than doing a spherical
average the number of atoms used here is 6912 (12x12x12 fcc).

The transverse and longitudinal current correlation is shown both in
time and frequency between :math:`\Gamma` and K. Here the
:math:`q`-value is between 0 and 1 and represent how far along the
path you are.

.. figure:: figs/Al_solid/C_ktw_GK.png
    :scale: 50 %
    :align: center
	
    Current correlations between :math:`\Gamma` and K in both time
    (top) and frequency .

Looking at the current correlations in time we see that the
longitudinal is oscillating with one frequency and the transverse with
many. This agrees with what is seen in the frequency domain.

The sum of the longitudinal and transverse current correlation is seen
below together with the phonon dispersion calculated, using the same
potential, by phonopy.

.. figure:: figs/Al_solid/dispersion_T300.png
    :scale: 50 %
    :align: center
	
    Sum of the longitudinal and transverse current show along the path
    L to :math:`\Gamma` to K. The black dots indicate the results from
    phonopy calculations.

The agreement with the phonon dispersion calculated with phonopy is
very good. The fact that there is discrete :math:`q`-values which give
rise to high intensitiy is due to the finite supercell size not
supporting all kinds of oscillations.


